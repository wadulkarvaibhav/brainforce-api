
import path from 'path';
import fs from 'fs';
import Utils from './utils';
import Database from './database';
import ResponseHandler from './responseHandler';
import {serverConfig} from '../../config';
import mongoose from 'mongoose';

export class Server {
	public static cache: any = {};
	public static config: any;
	public static _server: any;
	public static isTestMode: boolean;
	public static isUserMode: boolean = false;
	public static isInit: boolean;
	static _moduleRegExp: any = new RegExp('^([^.]+).(ts|js)$');
	public static model: any = {};
	
	public static async connectDatabse() {
		try {
			Server._server = serverConfig;
			let result = await Database.connect();
			return result;
		} catch (ex) {
			console.log(ex);
		}
	}

	public static async loadAPIRoutes(app: any) {
		// api controller routes
		const apiDir: string = path.join(Utils.rootDir(), 'src/api');
		if (fs.existsSync(apiDir)) {
			const subDir = fs.readdirSync(apiDir);
			subDir.forEach(function (dir: any) {
				const subDirPath = path.join(apiDir, dir);
				const apiFiles = fs.readdirSync(subDirPath);

				apiFiles.forEach(function (api: string) {
					if (fs.statSync(apiDir + '/' + dir + '/' + api).isDirectory()) {
						const files = fs.readdirSync(apiDir + '/' + dir + '/' + api);
						files.forEach(function (objName: string) {
							if (Server._moduleRegExp.test(objName)) {
								const parts = objName.match(Server._moduleRegExp);
								const apiname = (parts) ? parts[1] : '';
								const api_path = subDirPath + '/' + api + '/' + apiname;
								const controller = require(api_path);
								app.use('/api/' + dir + '/' + api + '/' + apiname, controller.default);
							}
						});
					} else {
						if (Server._moduleRegExp.test(api)) {
							const parts = api.match(Server._moduleRegExp);
							const apiname = (parts) ? parts[1] : '';
							const api_path = path.join(subDirPath, apiname);
							const controller = require(api_path);
							app.use('/api/' + dir + '/' + apiname, controller.default);
						}
					}
				});

			});
		}
		return ResponseHandler.success(null, `api routing is done successfully`);
	}

	public static async createDefaultUser() {
		Server.isInit = true;
		let result: any = await Database.save(Server._server.user);
		if(!result.success) {
			return result;
		}
		Server.isInit = false;
		Server.cache['auid'] = 
		Server._server.user['_id'] = 
		Server._server.user.owner = 
		Server._server.user.createdBy = result.data;
		result = await Database.save(Server._server.user);
		if(!result.success) {
			return result;
		}
		return ResponseHandler.success(null, `default user created successfully`);
	}

	public static async getModel(objType: any) {
		if(!objType) {
			return ResponseHandler.error(`specify object to get model`);
		}
		if(Server.model[objType]) {
			return ResponseHandler.success(Server.model[objType]);
		}
		const fileName = path.join(Utils.rootDir(), 'src/object/'+ objType );
		const objectSchema = require(fileName);
		Server.model[objType] = mongoose.model(objectSchema[objType]['name'], objectSchema[objType]['def']);		
		return ResponseHandler.success(Server.model[objType]);
	}

}
export default Server;