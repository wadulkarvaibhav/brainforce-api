import nodemailer from 'nodemailer';
import ErrorManager from './errorManager';
require('dotenv').config();

export class Email {
	private static async sendEmailUsingTemplateString(parsedTemplate: string) {
		const template = JSON.parse(parsedTemplate);
		const mailOptions = {
			from: template.from,
			to: template.to,
			subject: template.subject,
			text: template.text,
			html: template.body
		};
		return await Email.sendEmail(mailOptions);
	}

	private static getTransporter(): any {
		const email = {
			service: 'gmail',
			auth: {
				user: process.env.GMAIL_EMAIL,
				pass: process.env.GMAIL_PASSWORD
			}
		};
		return nodemailer.createTransport(email);
	}

	public static async sendEmail(mailOptions: any) {
		try {
			const transporter = this.getTransporter();
			const response = await transporter.sendMail(mailOptions);
			return ErrorManager.success(response);
		} catch (error) {
			return ErrorManager.error(error);
		}
	}

}

export default Email;
