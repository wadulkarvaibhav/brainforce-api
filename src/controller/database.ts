import { ResponseHandler } from './responseHandler';
import { MongoClient, ObjectId } from 'mongodb';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import Server from './server';
dotenv.config();

class Database {
	public static db: any;
	public static async connect() {
		try {
			// if (!Database.db) {
				const connection: any = await mongoose.connect(`${process.env.DB_CONNECTION_STRING}`, {
					useNewUrlParser: true,
					useUnifiedTopology: true
				});
				console.log('connection string: ' + process.env.DB_CONNECTION_STRING);
				// const con = await MongoClient.connect(`${process.env.DB_CONNECTION_STRING}`, { useUnifiedTopology: true });
				// Database.db = con.db();
				return ResponseHandler.success(connection, `database connection establised successfully`)
			// }
		} catch (ex) {
			return ResponseHandler.error(ex);
		}
	}
	public static async close() {
		try {
			if (Database.db) {
				await Database.db.close();
			}
		} catch (ex) {
			return ResponseHandler.error(ex);
		}
	}
	public static async save(records: any, context?: any) {

		try {
			const mapRecords: any = { 'insert': {}, 'update': {} };
			const isArray = Array.isArray(records);
			records = Array.isArray(records) ? records : [records];
			let admin: any;
			if(!Server.isInit) {
				admin = await Server.cache['auid'];
			}
			for (const record of records) {
				const event = record.hasOwnProperty('_id') ? 'update' : 'insert';
				if (record.objType) {
					if (!mapRecords[event].hasOwnProperty(record.objType)) {
						mapRecords[event][record.objType] = [];
					}
				}
				if (event === 'insert') {
					if (context && context.uid) {
						record.owner = record.createdBy = record.lastModifiedBy = context.uid;
					} else if (admin) {
						record.owner = record.createdBy = record.lastModifiedBy = admin._id;
					}
					record.isDeleted = false;
					record.createdOn = new Date();
				}
				record.lastModifiedOn = new Date();
				mapRecords[event][record.objType].push(record);
			}

			let retRecords: any = [];
			let result: any;
			if (Object.keys(mapRecords.insert).length > 0) {
				result = await Database.modify(mapRecords.insert, 'insert', context);
			}
			if (result && !result.success) {
				return ResponseHandler.error(result);
			}
			if (Object.keys(mapRecords.update).length > 0) {
				result = await Database.modify(mapRecords.update, 'update', context);
			}
			if (result.success) {
				retRecords = retRecords.concat(result.data);
			} else {
				retRecords = retRecords.concat(result);
			}
			if (isArray) {
				let isSuccess = true;
				for (const iIndex in retRecords) {
					if (!retRecords[iIndex].success) {
						isSuccess = false;
						break;
					}
				}
				return ResponseHandler[isSuccess ? 'success' : 'error'](retRecords);
			} else {
				if (retRecords.length > 0) {
					return retRecords[0];
				} else {
					return ResponseHandler.error(`one or more records failed to process`);
				}
			}
		} catch (ex) {
			return ResponseHandler.error(ex);
		}
	}
	static async modify(mapRecords: any, event: any, context?: any) {
		try {
			const arrRecords: any = [];
			for (const objectName in mapRecords) {
				if (mapRecords[objectName] && mapRecords[objectName].length > 0) {
					let result: any = await Server.getModel(objectName);
					if(!result.success) {
						return result;
					}
					if (event === 'insert') {
						for(const iIndex of Object.keys(mapRecords[objectName])) {
							mapRecords[objectName][iIndex]._id = new ObjectId().toString();
							const model: any = new result.data(mapRecords[objectName][iIndex]);
							const response = await model.save();
							if(!response){
								return ResponseHandler.error(`unable to save the records`);
							}
							arrRecords.push(ResponseHandler.success(mapRecords[objectName][iIndex]['_id'] = response._id));
						}
					} else {
						for (const iIndex of Object.keys(mapRecords[objectName])) {
							const model: any = new result.data(mapRecords[objectName][iIndex]);
							const response = await model.updateOne(
								{ _id: mapRecords[objectName][iIndex]._id },
								{
									$set: mapRecords[objectName][iIndex]
								}
							);
						
							if(!response){
								return ResponseHandler.error(`unable to save the records`);
							}
							arrRecords.push(ResponseHandler.success(mapRecords[objectName][iIndex]['_id'] = response._id));
						}
					}
				}
			}
			return ResponseHandler.success(arrRecords);
		} catch (ex) {
			return ResponseHandler.error(ex);
		}
	}
	public static async query(query: any) {
		try {
			if (!query.conditions) {
				query.conditions = {};
			}
			query.conditions['isDeleted'] = false;
			const pipeline = await Database.createPipeline(query);
			if (!pipeline.success) {
				return pipeline;
			}
			let result: any = await Database.db.collection(query.objType).aggregate(pipeline.data).toArray();
			return ResponseHandler.success(result);
		} catch (ex) {
			return ResponseHandler.error(ex);
		}
	}
	static async createPipeline(query: any) {
		let result: any = {};
		let projection: any = { _id: 1};
		if (query.fields) {
			projection = { ...projection, ...query.fields };
		}
		const pipeline: any = [];
		if (query.conditions && Object.keys(query.conditions).length > 0) {
			pipeline.push({ $match: query.conditions });
		}

		if (query.sort && Object.keys(query.sort).length > 0) {
			result = Database.constructSortCondition(query.sort);
			if (!result.success) {
				return result;
			}
			pipeline.push({ $sort: result.data });
		} else {
			pipeline.push({ $sort: { lastModifiedOn: -1 } });
		}
		if (query.offset) {
			pipeline.push({ $skip: query.offset });
		}
		if (query.limit) {
			pipeline.push({ $limit: query.limit });
		}
		return ResponseHandler.success(pipeline);
	}
	static constructSortCondition(sort: any) {
		const sortCondition: any = {};
		for (const Field of Object.keys(sort)) {
			if (sort[Field] !== 'asc' && sort[Field] !== 'desc') {
				return ResponseHandler.error(`invalid sort`);
			}
			sortCondition[Field] = sort[Field] === 'asc' ? 1 : -1;
		}
		return ResponseHandler.success(sortCondition);
	}
	public static async delete(records: any, context?: any) {
		try {
			const isArray = Array.isArray(records);
			records = Array.isArray(records) ? records :[records];
			const mapRecords: any = {};
			for (const record of records) {
				if (!mapRecords.hasOwnProperty(record.objType)) {
					mapRecords[record.objType] = [];
				}
				const deletedRecord: any = { objType: record.objType, _id: record._id };
				// Set audit fields
				if (context && context.uid) {
					deletedRecord.deletedBy = deletedRecord.lastModifiedBy = context.uid;
				} else {
					const admin = Server.cache['auid'];
					if (admin) {
						deletedRecord.deletedBy = deletedRecord.lastModifiedBy = admin._id;
					}
				}
				deletedRecord.deletedOn = deletedRecord.lastModifiedOn = new Date();
				deletedRecord.isDeleted = true;
				mapRecords[record.objType].push(deletedRecord);
			}
			let retRecords: any = [];
			let result: any;
			if (Object.keys(mapRecords).length > 0) {
				result = await Database.modify(mapRecords, 'delete');
				if (result.success) {
					retRecords = retRecords.concat(result.data);
				}
			}
			if (isArray) {
				let isSuccess = true;
				for (const iIndex in retRecords) {
					if (!retRecords[iIndex].success) {
						isSuccess = false;
						break;
					}
				}
				return ResponseHandler[isSuccess ? 'success' : 'error'](retRecords);
			} else {
				if (retRecords.length > 0) {
					return retRecords[0];
				} else {
					return ResponseHandler.error(`one or more records failed to delete`);
				}
			}
		} catch (ex) {
			return ResponseHandler.error(ex);
		}
	}
}
export default Database;