import path from 'path';

class Utils {
    private static _rootDir: string = '';
    public static rootDir() {
		if (Utils._rootDir) {
			return Utils._rootDir;
		}
		if (path.dirname(__dirname).endsWith('dist')) {
			return path.resolve(__dirname + './../../');
		}
		return path.resolve(__dirname + './../../');
	}
}
export default Utils;