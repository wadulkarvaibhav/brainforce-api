import i18n from 'i18n';
import * as os from 'os';

export class Result {
	public readonly success: boolean;
	public readonly message?: string;
	public readonly data?: any;
	public readonly stack?: string;
	public readonly hostname?: string;
	public readonly results?: Array<any>;
	public readonly code?: number;

	constructor(success: boolean, message?: string, data?: any, results?: any, code?: number) {
		this.success = success;
		this.hostname = os.hostname();
		if (data) {
			this.data = data;
		}

		if (success) {
			if (message) {
				this.message = message;
			} else { this.message = ''; }
			if (results) { this.results = results; }
		} else {
			if (results) {
				if (Array.isArray(results)) {
					this.results = results;
				} else if (typeof results === 'object') {
					if (results instanceof Error) {
						this.message = results.message;
						if (results.stack) {
							this.stack = results.stack;
						}
					}
				} else if (typeof results === 'string') {
					this.message = results;
				} else if (typeof results.toString === 'function') {
					this.message = results.toString();
				}
			}
		}
		if (code) {
			this.code = code;
		}
	}
}
export class ErrorManager {
	public static success(data: any, message?: string): any {
		return new Result(true, message, data);
	}

	public static error(error: any, data?: any, code?: number): any {
		// console.log(error);

		if ('[object Object]' === Object.prototype.toString.call(error)) {
			return new Result(false, undefined, data, error.message, code);
		} else {
			return new Result(false, undefined, data, error, code);
		}
	}

	public static multiRecordSuccess(results: Array<any>): any {
		const data: Array<any> = [];
		for (const result of results) {
			data.push(result.data);
		}
		return new Result(true, i18n.__('server.processedMultiRecords:one or more records processed successfully'), data, results);
	}

	public static multiRecordError(results: Array<any>): any {
		const data: Array<any> = [];
		for (const result of results) {
			data.push(result.data);
		}
		let message = i18n.__('server.oneOrMoreError:one or more error occurred');
		for (const result of results) {
			if (!result.success) {
				message += '\n' + result.message;
			}
		}
		return new Result(false, message, data, results);
	}
}
export default ErrorManager;
