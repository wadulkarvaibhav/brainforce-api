import i18n from 'i18n';
import Database from './database';
import ErrorManager from './errorManager';


require('dotenv').config();

export default class Authentication {
	/*register user*/
	public static async registerUser(user: any) {
		const verificationcode = { username: user.username, code: user.code };
		
		//  Validate user for email and phone
		if (!user.password) {
			return ErrorManager.error(i18n.__('authentication.missingPassword: No password specified for ther user'));
		}
	
		if (user.username) {
				user.email = user.username;
			} else {
				return ErrorManager.error(i18n.__('authentication.badUsername:Username seems to be invalid based on format'));
			}
		
		// Validate for duplicate user by username
		const result = await Database.query({
			objType: 'user',
			conditions: {
				username: user.username,
				isVerified: true
			}
		});

		if (result.success && result.data.length > 0) {
			return ErrorManager.error(i18n.__('authentication.userAlreadyExists:User with username {{username}} already exists', user));
		}
		const originalPassword = user.password;

		try {
			if (user) {
				const result: any = await Database.save(user);
				if (!result.success) {
					return result;
				} 
			}
			//return await this.login({ username: user.username, password: originalPassword });
		} catch (error) {
			return ErrorManager.error(error);
		}
	}

	// public static async prepareUser(user: any) {
	// 	user.salt = Utils.generateSalt();
	// 	user.password = Utils.sha256(user.password, user.salt);
	// 	user.isActive = true;
	// 	user.objType = 'user';
	// 	if (user.firstName && user.lastName) {
	// 		user.name = user.firstName + ' ' + user.lastName;
	// 	} else if (user.lastName) {
	// 		user.name = user.lastName;
	// 	} else {
	// 		user.name = user.username;
	// 	}
	// 	const settings: any = await Utils.getSettings();
	// 	if (settings) {
	// 		user.passwordExpiresOn = Utils.setExpiresOn(settings.defaultPasswordExpiration * 24 * 60);
	// 	}
	// 	if (!user.passwordHistory) {
	// 		user.passwordHistory = [];
	// 	}
	// 	user.passwordHistory.push(user.password);
	// 	user.invalidLoginAttempts = 0;
	// 	delete user['code'];
	// 	delete user['emailOrMobile'];
	// 	return user;
	// }

	// /*login user*/
	// public static async login(user: any) {
	// 	let username: any;
	// 	let hashVerified = false;
	// 	if (user.hashcode) {
	// 		const hashResult = await Database.query({
	// 			objType: 'hashcode',
	// 			conditions: { code: user.hashcode },
	// 			fields: ['username']
	// 		});
	// 		if (hashResult.success && hashResult.data.length > 0) {
	// 			username = hashResult.data[0].username;
	// 			hashVerified = true;
	// 		}
	// 	}
	// 	if (!username) {
	// 		if (!user.username) {
	// 			return ErrorManager.error(i18n.__('login.missingUsername:Username is required'));
	// 		}
	// 		if (!user.password) {
	// 			return ErrorManager.error(i18n.__('login.missingPassword:Password is required'));
	// 		}
	// 		username = user.username;
	// 	}
	// 	const settings: any = await Utils.getSettings();
	// 	let result = await Database.query({
	// 		objType: 'user',
	// 		conditions: { username: username },
	// 		fields: ['password', 'salt', 'isTwoFactorEnabled', 'invalidLoginAttempts', 'effectiveLockoutPeriod']
	// 	});
	// 	const record = result.data[0];
	// 	if (result.success && result.data.length > 0) {
	// 		if (record.effectiveLockoutPeriod && record.effectiveLockoutPeriod > new Date()) {
	// 			const lockoutPeriodRemains = Utils.getdateDifferenceInSec(record.effectiveLockoutPeriod, new Date());
	// 			let timeRemains;
	// 			if (lockoutPeriodRemains < 60) {
	// 				timeRemains = lockoutPeriodRemains + ' seconds';
	// 			} else {
	// 				timeRemains = Math.round(lockoutPeriodRemains / 60) + ' minutes';
	// 			}
	// 			return ErrorManager.error('Try again after ' + timeRemains);
	// 		}
	// 		if (hashVerified || (record.password === Utils.sha256(user.password, record.salt))) {
	// 			let hashcode: any;
	// 			if (user.rememberMe && !hashVerified) {
	// 				hashcode = Utils.generateSalt();
	// 				const result = await Database.save({ objType: 'hashcode', username: user.username, code: hashcode });
	// 				if (!result.success) {
	// 					return result;
	// 				}
	// 			}
	// 			let body: any = {};
	// 			if (!record.isTwoFactorEnabled) {
	// 				delete record.password;
	// 				delete record.salt;
	// 				const sessionResult = await Authentication.createSession(record);
	// 				if (!sessionResult.success) {
	// 					return sessionResult;
	// 				}
	// 				body = { sid: sessionResult.data };
	// 			} else if (record.isTwoFactorEnabled) {
	// 				body = { twoFactorEnabled: true, username: username };
	// 			}
	// 			if (user.rememberMe) {
	// 				body.hashcode = hashcode;
	// 			}
	// 			// update users invalidLoginAttempts to zero
	// 			record.invalidLoginAttempts = 0;
	// 			record.effectiveLockoutPeriod = undefined;
	// 			result = await Database.save(record);
	// 			return ErrorManager.success(body);
	// 		} else {
	// 			// increase users invalid login attempts by 1
	// 			record.invalidLoginAttempts = record.invalidLoginAttempts + 1;
	// 			const invalidLoginAttempts = Object.keys(settings.invalidLoginAttemptsAndLockouttPeroid);
	// 			let isUserLocked = false;
	// 			for (let index = 0; index < invalidLoginAttempts.length; index++) {
	// 				if (record.invalidLoginAttempts < invalidLoginAttempts[0]) {
	// 					break;
	// 				}
	// 				if (record.invalidLoginAttempts.toString() === invalidLoginAttempts[index].toString() && settings.invalidLoginAttemptsAndLockouttPeroid[record.invalidLoginAttempts]) {
	// 					record.effectiveLockoutPeriod = Utils.setExpiresOn(settings.invalidLoginAttemptsAndLockouttPeroid[record.invalidLoginAttempts]);
	// 					isUserLocked = true;
	// 					break;
	// 				}
	// 			}
	// 			delete record.password;
	// 			delete record.salt;
	// 			result = await Database.save(record);
	// 			if (!result.success) {
	// 				console.log('Error while updating users invalidLoginAttempts');
	// 			}
	// 			if (isUserLocked) {
	// 				return ErrorManager.error('Try again after ' + settings.invalidLoginAttemptsAndLockouttPeroid[record.invalidLoginAttempts] + ' minutes');
	// 			}
	// 		}
	// 	}
	// 	return ErrorManager.error(i18n.__('authentication.invalidUsernamePassword:Invalid Username or Password'));
	// }

	// /*This method checks if the given username is unique or not*/
	// public static async checkUserExists(username: string) {
	// 	const result = await Database.query({
	// 		conditions: { username: username, isVerified: true },
	// 		objType: 'user'
	// 	});
	// 	if (result.success && result.data.length > 0) {
	// 		return ErrorManager.success(i18n.__('authentication.userAlreadyExists:User already exists'));
	// 	} else {
	// 		return ErrorManager.error(i18n.__('authentication.userDoesNotExist:User does not exists'));
	// 	}
	// }

	// public static async authenticateSession(req: any, skipSessionSliding?: boolean) {
	// 	const certificate = '3MVG9lKcPoNINVBIPJjdw1J9LLM82Hn';
	// 	let context: any;
	// 	const authorization: any = req.headers.authorization ? JSON.parse(req.headers.authorization) : {};
	// 	if (req.headers.authorization && authorization && authorization.crt === certificate) {
	// 		return ErrorManager.success(i18n.__('authentication.validUser:User authenticated successfully'));
	// 	}
	// 	authorization.sid = authorization.sid ? authorization.sid : undefined;
	// 	if (!authorization.sid) {
	// 		return ErrorManager.error(i18n.__('authentication.missingSID:No sid sent to be authenticated'), '', 401);
	// 	}
	// 	delete authorization['iid'];
	// 	// Cache.set('authorization', authorization);
	// 	let result: any = await Database.query({
	// 		objType: 'session',
	// 		fields: ['_id', 'expiresOn', 'user', 'instance'],
	// 		conditions: {
	// 			_id: authorization.sid,
	// 			expiresOn: { $gt: new Date() }
	// 		}
	// 	});
	// 	if (result.success && result.data.length > 0) {
	// 		const session: any = result.data[0];
	// 		if (!skipSessionSliding) {
	// 			const settings = await Utils.getSettings();
	// 			result = await Database.save({
	// 				objType: 'session',
	// 				_id: authorization.sid,
	// 				expiresOn: Utils.setExpiresOn(settings.defaultSessionExpiration),
	// 			});
	// 			if (!result.success) {
	// 				return result;
	// 			}
	// 		}
	// 		// Cache.set('session', session);
	// 		context = { sid: session._id, uid: session.user };
	// 		return ErrorManager.success(context, i18n.__('authentication.validUser:User session authenticated successfully'));
	// 	}
	// 	return (result.success && result.data.length > 0) ? ErrorManager.error(i18n.__('authentication.sessionExpired:session is expired'), '', 401) :
	// 		ErrorManager.error(i18n.__('authentication.invalid:session is invalid'), '', 401);
	// }

	// public static async authenticateInstance(req: any) {
	// 	const context: any = {};
	// 	const authorization: any = req.headers.authorization ? JSON.parse(req.headers.authorization) : {};
	// 	authorization.iid = authorization.iid ? authorization.iid : undefined;
	// 	authorization.sid = authorization.sid ? authorization.sid : undefined;
	// 	let user: any;
	// 	if (!authorization.iid) {
	// 		return ErrorManager.error(i18n.__('authentication.missingIID:No iid sent to be authenticated'));
	// 	}
	// 	context.iid = authorization.iid;
	// 	let result: any = await Database.query({
	// 		objType: 'session',
	// 		fields: ['_id', 'expiresOn', 'user', 'instance'],
	// 		conditions: {
	// 			_id: authorization.sid,
	// 			expiresOn: { $gt: new Date() }
	// 		}
	// 	});
	// 	if (!result.success || result.data.length === 0) {
	// 		return ErrorManager.error((result.message && result.message !== '') ? result.message : 'No session found for given user.');
	// 	}
	// 	const session: any = result.data[0];
	// 	if (authorization.iid) {
	// 		result = await Database.query({
	// 			objType: 'member',
	// 			fields: ['_id', 'profile'],
	// 			conditions: {
	// 				user: session.user,
	// 				instance: authorization.iid
	// 			}
	// 		});
	// 		if (!result.success || result.data.length === 0) {
	// 			return ErrorManager.error((result.message && result.message !== '') ? result.message : 'No member found for given user.');
	// 		}
	// 		result = await Database.query({
	// 			objType: 'profile',
	// 			fields: ['_id', 'name', 'objectPermissions', 'applicationPermissions', 'isAdmin'],
	// 			conditions: {
	// 				name: result.data[0].profile,
	// 				instance: authorization.iid
	// 			}
	// 		});
	// 		if (!result.success || result.data.length === 0) {
	// 			return ErrorManager.error((result.message && result.message !== '') ? result.message : 'No profile found for given user.');
	// 		}
	// 		context.profile = result.data[0];
	// 		user = await Database.query({
	// 			objType: 'user',
	// 			conditions: {
	// 				_id: session.user,
	// 				isActive: true
	// 			},
	// 			fields: ['_id', 'username', 'firstName', 'lastName', 'email', 'phone']
	// 		});
	// 		if (!user.success || user.data.length === 0) {
	// 			return ErrorManager.error((result.message && result.message !== '') ? result.message : 'No user found.');
	// 		}
	// 		context.user = user.data[0];
	// 	}
	// 	return ErrorManager.success(context, i18n.__('authentication.validUser:User instance authenticated successfully'));
	// }
	// /*authenticate user */
	// public static async authenticateRequest(req: any, event?: string) {
	// 	const certificate = '3MVG9lKcPoNINVBIPJjdw1J9LLM82Hn';
	// 	let context: any;
	// 	const authorization: any = req.headers.authorization ? JSON.parse(req.headers.authorization) : {};
	// 	if (req.headers.authorization && authorization && authorization.crt === certificate) {
	// 		context = { crt: certificate };
	// 		return ErrorManager.success(context, i18n.__('authentication.validUser:User authenticated successfully'));
	// 	}
	// 	authorization.sid = authorization.sid ? authorization.sid : undefined;
	// 	if (!authorization.sid) {
	// 		return ErrorManager.error(i18n.__('authentication.missingSID:No sid sent to be authenticated'), '', 401);
	// 	}

	// 	let result: any = await Authentication.authenticateSession(req);
	// 	if (!result.success) {
	// 		return result;
	// 	}
	// 	context = result.data;
	// 	result = await Authentication.authenticateInstance(req);
	// 	if (!result.success) {
	// 		return result;
	// 	}
	// 	context.iid = result.data.iid;
	// 	context.pid = result.data.profile._id;
	// 	context.profile = result.data.profile;
	// 	context.user = result.data.user;
	// 	if (event) {
	// 		if (event.endsWith('Model')) {
	// 			const modelResult = await Database.query({ objType: 'model', fields: ['modelDef.datasource', 'modelDef.object'], conditions: { name: req.body.model } });
	// 			if (!modelResult.success || (modelResult.data && (modelResult.data.length === 0))) {
	// 				return ErrorManager.error(i18n.__('authentication.validModelName:Model name does not exist'));
	// 			}
	// 			const adapterResult = await Database.query({ objType: 'datasource', fields: ['adapter'], conditions: { name: modelResult.data[0]['modelDef.datasource'] } });
	// 			if (!adapterResult.success) {
	// 				return adapterResult;
	// 			}
	// 			if (adapterResult.data[0]['adapter'] !== 'appbuilder') {
	// 				return ErrorManager.success(context);
	// 			}
	// 			if (event === 'saveModel') {
	// 				if (Array.isArray(req.body.records)) {
	// 					req.body.records.forEach((record: any) => {
	// 						record.objType = modelResult.data[0]['modelDef.object'];
	// 					});
	// 				} else {
	// 					req.body.records.objType = modelResult.data[0]['modelDef.object'];
	// 				}
	// 			}
	// 		} else if (event.endsWith('Report')) {
	// 			const reportResult = await Database.query({ objType: 'report', conditions: { name: req.body.report.name } });
	// 			if (!reportResult.success || (reportResult.data && (reportResult.data.length === 0))) {
	// 				return ErrorManager.error(i18n.__('authentication.validModelName:Report name does not exist'));
	// 			}
	// 			return ErrorManager.success(context);
	// 		}
	// 	}
	// 	return ErrorManager.success(context, i18n.__('authentication.validUser:User authenticated successfully'));
	// }

	// /*update the password after verificationCode is validated */
	// public static async updatePassword(body: any) {
	// 	try {
	// 		if (body.verificationCode) {
	// 			const result = await Verify.validateVerificationCode(body.verificationCode);
	// 			if (result.success) {
	// 				const record = await Database.query({
	// 					objType: 'user',
	// 					conditions: { username: body.verificationCode.username },
	// 					fields: ['salt', 'passwordHistory']
	// 				});
	// 				if (!record.success) {
	// 					return ErrorManager.error(i18n.__('authentication.userNotFound:User with this username does not exists'));
	// 				}

	// 				const settings: any = await Utils.getSettings();
	// 				if (body.password.length < settings.minPasswordLength) {
	// 					return ErrorManager.error(i18n.__('authentication.wrongPasswordLength: Password length does not match.'));
	// 				}
	// 				const passwordPattern = new RegExp(settings.passwordPattern);
	// 				if (!passwordPattern.test(body.password)) {
	// 					return ErrorManager.error(i18n.__('authentication.wrongPasswordFormat: Password pattern does not match.'));
	// 				}
	// 				const user = record.data[0];
	// 				let isCurrentPwdSameAsPrev = false;
	// 				user.passwordHistory.forEach((prevPwdSalt: any) => {
	// 					if (prevPwdSalt === Utils.sha256(body.password, user.salt)) {
	// 						isCurrentPwdSameAsPrev = true;
	// 					}
	// 				});
	// 				if (isCurrentPwdSameAsPrev) {
	// 					return ErrorManager.error(i18n.__('authentication.sameAsPrevPassword: New password cannot be same as the previous password. Try again with another password.'));
	// 				}
	// 				user.password = Utils.sha256(body.password, record.data[0].salt);
	// 				user.passwordExpiresOn = Utils.setExpiresOn(settings.defaultPasswordExpiration * 24 * 60);
	// 				user.passwordHistory.push(user.password);
	// 				if (user.passwordHistory.length > settings.numberOfPasswordsRemembered) {
	// 					user.passwordHistory.splice(0, 1);
	// 				}
	// 				user.invalidLoginAttempts = 0;
	// 				user.effectiveLockoutPeriod = undefined;
	// 				const result: any = await Database.save(user);
	// 				if (result.success) {
	// 					await Verify.deleteVerificationCode(body.verificationCode);
	// 					return ErrorManager.success('', i18n.__('authentication.updatePassord.success: Password updated successfully.'));
	// 				}
	// 				return result;
	// 			} else {
	// 				return ErrorManager.error(i18n.__('authentication.invalidVerificationCode:Invalid verification code'));
	// 			}
	// 		} else {
	// 			return ErrorManager.error(i18n.__('authentication.verificationCodeMissing:Enter verification code'));
	// 		}
	// 	} catch (error) {
	// 		console.log('Error : Update password : ', error);
	// 		return ErrorManager.error(error);
	// 	}
	// }

	// /*sends the forgot password link to the user*/
	// public static async forgotPassword(body: any) {
	// 	try {
	// 		if (!(Utils.getRegex('email').test(body.username) || Utils.getRegex('phone').test(body.username))) {
	// 			return ErrorManager.error(i18n.__('authentication.badUsername:Username seems to be invalid based on format'));
	// 		}
	// 		return await Verify.verificationCode(body.username);
	// 	} catch (error) {
	// 		return ErrorManager.error(error);
	// 	}
	// }

	// /*deletes the specified session*/
	// public static async deleteSession(sessionId: string) {
	// 	//  delete the session from database
	// 	const result: any = await Database.delete({
	// 		_id: sessionId,
	// 		objType: 'session'
	// 	});
	// 	if (result.success && result.data.length > 0) {
	// 		Cache.delete('session');
	// 		return ErrorManager.success(i18n.__('authentication.logout.successMeassage:Session deleted successfully'), sessionId);
	// 	}
	// 	return ErrorManager.error(i18n.__('authentication.logout.errorMeassage:Unable to delete the session'), sessionId);
	// }

	// /*resets the password of the given user, functionality to send that to user is remaining*/
	// public static async resetPassword(body: any) {
	// 	try {
	// 		if (body.password && body.oldPassword) {
	// 			let result: any = await Database.query({
	// 				objType: 'user',
	// 				fields: ['salt', 'password'],
	// 				conditions: {
	// 					username: body.username
	// 				}
	// 			});
	// 			if (result.success && result.data.length > 0) {
	// 				const user = result.data[0];
	// 				// Validate user for old password.
	// 				if (Utils.sha256(body.oldPassword, user.salt) !== user.password) {
	// 					return ErrorManager.error(i18n.__('authentication.invalidOldPassword:You have entered invalid old password.'));
	// 				}
	// 				// Old password and new password can not be same
	// 				if (Utils.sha256(body.password, user.salt) === user.password) {
	// 					return ErrorManager.error(i18n.__('authentication.oldPasswordAndNewPasswordShouldBeDifferent'));
	// 				}
	// 				// Update user with new password with salt encryption
	// 				const settings: any = await Utils.getSettings();
	// 				result = await Database.save({
	// 					_id: user._id,
	// 					objType: user.objType,
	// 					password: Utils.sha256(body.password, user.salt),
	// 					passwordExpiresOn: Utils.setExpiresOn(settings.defaultPasswordExpiration * 24 * 60)
	// 				});
	// 				delete result.password;
	// 				delete result.salt;
	// 				return result;
	// 			} else {
	// 				return ErrorManager.error(i18n.__('authentication.noUserFound:User with username {{username}} does not exist.', body));
	// 			}
	// 		} else {
	// 			return ErrorManager.error(i18n.__('authentication.oldPasswordorNewPasswordIsMissing'));
	// 		}
	// 	} catch (error) {
	// 		return ErrorManager.error(error);
	// 	}
	// }

	// /*creates a new session for given user*/
	// public static async createSession(user: any) {
	// 	const settings: any = await Utils.getSettings();
	// 	const session: any = {
	// 		objType: 'session',
	// 		user: user._id,
	// 		expiresOn: Utils.setExpiresOn(settings.defaultSessionExpiration),
	// 	};
	// 	const result: any = await Database.save(session);
	// 	if (!result.success) {
	// 		return ErrorManager.error(i18n.__('authentication.createSession.errorMessage:Unable to create the session'));
	// 	}
	// 	return ErrorManager.success(result.data);
	// }

	// /*login user by datasource*/
	// public static async oAuthlogin(datasource: any) {
	// 	if (!datasource.accessToken) {
	// 		return ErrorManager.error(i18n.__('login.missingAccessToken: No access token found'));
	// 	}
	// 	if (!datasource.refreshToken) {
	// 		return ErrorManager.error(i18n.__('login.missingRefreshToken: No refresh token founc'));
	// 	}

	// 	const req: any = { 'headers': {} };
	// 	req['headers']['authorization'] = JSON.stringify({
	// 		sid: datasource['accessToken'],
	// 		iid: datasource['datasourceInstance']
	// 	});

	// 	const validateSession: any = await Authentication.authenticateSession(req);

	// 	if (!validateSession.success && !validateSession.data) {
	// 		// refresh token flow
	// 		let result: any = await Database.query({
	// 			objType: 'token',
	// 			fields: ['user', 'refreshToken'],
	// 			conditions: {
	// 				refreshToken: datasource.refreshToken
	// 			}
	// 		});
	// 		if (!result.success && result.data.length === 0) {
	// 			return result;
	// 		}

	// 		const token = result.data[0];

	// 		result = await OAuth2Provider.createSession(token.user);
	// 		if (!result.success) {
	// 			return result;
	// 		}
	// 		const session = result;
	// 		token.accessToken = session.data;
	// 		token.issuedAt = new Date();

	// 		// update token record
	// 		result = await Database.save(token);
	// 		if (!result.success) {
	// 			return result;
	// 		}

	// 		// update datasource record with new accessToken
	// 		datasource.accessToken = session.data.toString();
	// 		result = await Database.save(datasource);
	// 		if (!result.success) {
	// 			return result;
	// 		}
	// 		return session;
	// 	} else {
	// 		return ErrorManager.success(validateSession.data.sid);
	// 	}
	// }

	// public static async createContextByMemberId(memberId: any, context: any) {
	// 	const profileRecord = await Database.query({ 'objType': 'profile', fields: ['isAdmin', 'instance', 'name', 'objType'], conditions: { 'instance': context.iid, '_id': context.pid }, context });
	// 	const isAdmin = profileRecord.success && (profileRecord.data[0]).hasOwnProperty('isAdmin') && (profileRecord.data[0])['isAdmin'] ? true : false;
	// 	if (!isAdmin) {
	// 		return ErrorManager.error('User has not suficient access');
	// 	}
	// 	const memberRecord = await Database.query({ 'objType': 'member', fields: ['profile._id', 'profile.isAdmin', 'profile.instance', 'profile.name', 'profile', 'user', 'user.firstName', 'user.lastName', 'user.instance', 'user.username', 'instance', ], conditions: { '_id': memberId }, context });
	// 	if (!memberRecord.success || memberRecord.data.length === 0) {
	// 		return ErrorManager.error('User has not suficient access');
	// 	}
	// 	context.profile.instance = (memberRecord.data[0])['profile.instance'];
	// 	context.profile.isAdmin = (memberRecord.data[0])['profile.isAdmin'];
	// 	context.profile.name = (memberRecord.data[0])['profile.name'];
	// 	context.profile._id = (memberRecord.data[0])['profile._id'];

	// 	context.user._id = (memberRecord.data[0])['user'];
	// 	context.user.instance = (memberRecord.data[0])['user.instance'];
	// 	context.user.username = (memberRecord.data[0])['user.username'];
	// 	context.user.firstName = (memberRecord.data[0])['user.firstName'];
	// 	context.user.lastName = (memberRecord.data[0])['user.lastName'];

	// 	context.iid = (memberRecord.data[0])['instance'];
	// 	context.pid = (memberRecord.data[0])['profile._id'];
	// 	context.uid = (memberRecord.data[0])['user'];
	// 	return ErrorManager.success(context);
	// }

}

