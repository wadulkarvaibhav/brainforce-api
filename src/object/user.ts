import { ObjectId } from 'mongodb';
export let user = {
	'name': 'user',
	'def': {
        _id: {
            type: String
        },
		firstName: {
			type: String
		},
		lastName: {
			type: String,
		},
		gender: {
			type: String,
			enum: ['male', 'female', 'other']
		},
		isActive: {
			type: String
		},
		username: {
			type: String
		},
		email: {
			type: String
		},
		phone: {
			type: String
		},
		password: {
			type: String
		},
		salt: {
			type: String
		}
	}
};

export default user;
