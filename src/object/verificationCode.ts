import { ObjectId } from 'mongodb';
export let verificationCode = {
	'name': 'verificationCode',
	'def': {
        _id: {
            type: String
        },
		code: {
			name: 'code',
			label: 'Code',
			type: 'string',
			required: true
		},
		username: {
			name: 'username',
			label: 'Username',
			type: 'string',
			required: true
		},
		expiresOn: {
			name: 'expiresOn',
			label: 'Expires On',
			type: 'datetime',
			required: true
		}
	}
};

export default user;
