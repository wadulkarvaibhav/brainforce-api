import express from 'express';
import Authentication from '../../controller/authentication';
import ErrorManager from '../../controller/errorManager';
import Database from '../../controller/database';
import Email from '../../controller/email';
export const router = express.Router();
export default router;

router.post('/',async function(req: any, res: any, next:any){
    try {
		if (!req.body.username ) {
			return res.json(ErrorManager.error(i18n.__('validateCode.missingUsername:Username is required.')));
        }
        let code = Math.floor((Math.random() * 999999) + 111111).toString();
		let result: any = await Database.save([{
            objType : 'verificationCode',
            username : req.body.username,
            code : code
        }]);
        if(!result.success){
            return res.json(ErrorManager.error('failed to save verification code.'));
        }
        let mailOptions: any = {
            from: 'Brain Force',
			to: req.body.username,
			subject: 'Verification Code',
			text: '',
			html: code
        }
        result = await Email.sendEmail(mailOptions)
        if(!result.success){
            return res.json(ErrorManager.error('failed to send email.'));
        }

	} catch (error) {
		return res.json(ErrorManager.error(error.message));
	}
})

router.get('/', async function (req: any, res: any, next: any) {
    res.send('get reqest on /verification');
    });