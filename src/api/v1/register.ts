import express from 'express';
import Authentication from '../../controller/authentication';
import ErrorManager from '../../controller/errorManager';

export const router = express.Router();
export default router;

router.post('/', async function (req: any, res: any, next: any) {
	try {
		const result: any = await Authentication.registerUser(req.body);
		if (result.success) {
			res.cookie('sid', result.data.sid);
		}
		res.json(result);
	} catch (error) {
		res.json(ErrorManager.error(error.message));
	}
});


router.get('/', async function (req: any, res: any, next: any) {
res.send('get reqest on /register');
});