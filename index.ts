import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import path from 'path';
require('dotenv').config();
export let app: any = express();
require('dotenv').config();

import Server from './src/controller/server';
app.get('/', function (req: any, res: any) {
    res.send(`API's Running!`);
});
 class App {
     public static async start() {
         let result: any = {};
        App.enableCORS();
        var server = await app.listen(process.env.PORT);
        console.log(`app listening on port ${process.env.PORT}!`)
        result = await Server.connectDatabse();
        if(!result.success) {
            return result;
        }
        console.log(result.message);
        result = await Server.loadAPIRoutes(app);
        if(!result.success) {
            return result;
        }
        console.log(result.message);
        result = await Server.createDefaultUser();
        if(!result.success) {
            return result;
        }
        console.log(result.message);
     }
     public static enableCORS() {
        app.options('*', cors());
        app.use(function (req: any, res: any, next: any) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, PATCH, DELETE, OPTIONS');
            res.header('Access-Control-Allow-Methods', 'Content-Type, Authorization');
            next();
        });
        app.use(express.json());
        app.use(express.urlencoded({
            extended: false
        }));
        app.use(cookieParser());
        app.use(express.static(path.join(__dirname, 'public')));
        
     }
 }
 export default App;




